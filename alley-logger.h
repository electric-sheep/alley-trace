/*
 * alley-logger.h
 *
 *  Created on: Oct 16, 2013
 *      Author: victor, jiri
 *
 *      This simple logger header is based on the examples codes that Andrey Shemashev had provided whith the
 *      Boost.Log library. All this examples can be found on this link:
 *      https://github.com/boostorg/log/blob/master/example/basic_usage/main.cpp
 *
 *      We define DEBUG severity level because we are interested in getting the information when debugging.
 *
 * Last modification commit: Nov 8, 2023
 */

#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>


#include <boost/log/core.hpp>
#include <boost/log/common.hpp>
#include <boost/log/expressions.hpp>

#include <boost/log/utility/setup/file.hpp>
#include <boost/core/null_deleter.hpp>
#include <boost/log/utility/manipulators/add_value.hpp>

#include <boost/log/attributes/mutable_constant.hpp>
#include <boost/log/attributes/counter.hpp>
#include <boost/log/sources/severity_logger.hpp>

#include <boost/log/sinks/text_ostream_backend.hpp>

#ifndef LOGGER_DISABLE
#define LOGGER_DISABLE false
#endif

#if LOGGER_DISABLE
#define LE std::cerr
#define LW std::cout
#define LI std::cout
#define LD std::cout
#else

//Regular logger without file and line information. The severity_logger has to be set manually
#define LG(sv) BOOST_LOG_SEV(glob_logger::get(), sv)



//Defining macros to use different severity level
//These macros include the file and line where the log has been made.

//TODO: There is a problem where long __FILE__ causes a linker warning about overflow in boost::log::add_value
/*
#define LE BOOST_LOG_SEV(glob_logger::get(),SL_ERROR) \
    << boost::log::add_value("RecordFile", __FILE__) \
    << boost::log::add_value("RecordLine", __LINE__)

#define LW BOOST_LOG_SEV(glob_logger::get(),SL_WARNING) \
    << boost::log::add_value("RecordFile", __FILE__) \
    << boost::log::add_value("RecordLine", __LINE__)
*/

#define LE BOOST_LOG_SEV(glob_logger::get(),SL_ERROR) \
    << boost::log::add_value("RecordLine", __LINE__)  \
    << __FILE__ << ": "
#define LW BOOST_LOG_SEV(glob_logger::get(),SL_WARNING) \
    << boost::log::add_value("RecordLine", __LINE__)    \
    << __FILE__ << ": "

#define LI BOOST_LOG_SEV(glob_logger::get(),SL_INFO)

#ifdef NDEBUG
#define LD if (false) std::cout
#else
#define LD BOOST_LOG_SEV(glob_logger::get(),SL_DEBUG)
#endif

#endif


namespace alley_trace
{

/****************************************************************************************************************************************
 * Basics definitions:
 *
 * In this section we define some important things that we will use with the logger, as the severity_level enum. We could also define here some typedefs if
 * we finally need them.
 * *************************************************************************************************************************************/
//Typedef for back and front ends
typedef boost::log::sinks::text_ostream_backend our_backend;
typedef boost::log::sinks::synchronous_sink<our_backend> our_frontend;

/** We define this mutable_constant<int> in order to set the rank order
 *   Definitions of different variables that will be used to check the order of the message, the directory and
 *   the name of the log file. */
extern int mpi_rank_order;

/** In order to set properly the place in which the log file will be
 *  stored, we define this string. It is initialized
 *  as empty string and it will only be changed if needed. */
static std::string logger_output_dir;

// Here we define our application severity levels.
enum severity_level
{
    SL_DEBUG,
    SL_INFO,
    SL_WARNING,
    SL_ERROR
};

//The next code is for make the severity level in a human-readable form
inline std::ostream& operator<< (std::ostream& strm, severity_level level)
{
    static const char* strings[] =
    {
        "DEBUG",
        "INFO",
        "WARNING",
        "ERROR"
    };

    if (static_cast< std::size_t >(level) < sizeof(strings) / sizeof(*strings))
        strm << strings[level];
    else
        strm << static_cast< int >(level);

    return strm;
}

/******************************************************************************************************************************************************************
 * Logger properties
 *
 * In this section we start defining properties the logger should have: filters, attributes, streams where the logger will write to...
 * Everything related with the logger logic and implementation.
 * ***************************************************************************************************************************************************************/
/* GLOBAL ATTRIBUTES KEYWORDS:
 *  This macro defines the keyword words as attribute identifiers we will use in the formatter.
 *  Doing so we avoid typing and the code is less error-prone. And we can change attr types
 *  (if necessary) without effort*/
BOOST_LOG_ATTRIBUTE_KEYWORD(_scope, "Scope", boost::log::attributes::named_scope::value_type)
BOOST_LOG_ATTRIBUTE_KEYWORD(_lineID, "RecordLine", int)
BOOST_LOG_ATTRIBUTE_KEYWORD(_fileID, "RecordFile", std::string)
BOOST_LOG_ATTRIBUTE_KEYWORD(_severity, "Severity", severity_level)
BOOST_LOG_ATTRIBUTE_KEYWORD(_rank, "Rank", int)

/** The logger will use only .h file (without implementation file or .cpp), so it is needed to apply the next macro
 *  instead of BOOST_LOG_GLOBAL_LOGGER_INIT because if this one is used, a .cpp file for defining it would be compulsory,
 *  not in the header (in which only would appear
 *  BOOST_LOG_GLOBAL_LOGGER(glob_logger, boost::log::sources::severity_logger_mt<severity_level>) in order to
 *  avoid multiple definitions. But as is said here
 *  http://boost-log.sourceforge.net/libs/log/doc/html/log/detailed/sources.html#log.detailed.sources.global_storage
 *  using the BOOST_LOG_INLINE_GLOBAL_LOGGER_INIT allows us to use only .hpp files*/

/** This macro defines a multiprocess secure singleton for the logger. Doing that we avoid problems with
 *  multiple proccesses in MPI trying to use the same logger, or trying to write to the same log file
 *  at the same time, with would led to several problems.*/

BOOST_LOG_INLINE_GLOBAL_LOGGER_INIT(glob_logger,
        boost::log::sources::severity_logger_mt<severity_level>)
{

    //Now we instantiate the core of the logger in order to register in it all the attributes and
    //back and front ends.
    boost::shared_ptr<boost::log::core> core = boost::log::core::get();

    //We check if logging is activated or deactivated
    if(LOGGER_DISABLE){
        core->set_logging_enabled(false);
    }else{
       	boost::log::attributes::counter<unsigned int> _counter(1);
       	boost::log::attributes::mutable_constant<int> rank_order_log(mpi_rank_order);
       	/*Backends defined for two streams: std::cout and file log.*/
       	boost::shared_ptr<our_backend> file_backend = boost::make_shared<our_backend>();
       	boost::shared_ptr<our_backend> debug_backend = boost::make_shared<our_backend>();
       	boost::shared_ptr<our_backend> cout_backend = boost::make_shared<our_backend>();
       	boost::shared_ptr<our_backend> cerr_backend = boost::make_shared<our_backend>();

       	std::string warn_file_name_;
       	std::string debug_file_name_;
       	/*We will define the place in which the log file will be stored only if output_dir
       	 * is not empty*/
       	if(logger_output_dir.length() > 1){
       	    warn_file_name_= logger_output_dir + '/' + "warn.log";
       	    debug_file_name_= logger_output_dir + '/' + "debug.log";
       	}
       	else{
       	    warn_file_name_="warn.log";
       	    debug_file_name_="debug.log";
       	}

       	/***************************Streams Definitions******************************
	     * Defining the streams. The stream associated to the file receives the name of the file
    	 * and the open mode append*/
       	boost::shared_ptr<std::ostream> screen_out(&std::cout, boost::null_deleter());
       	boost::shared_ptr<std::ostream> error_out(&std::cerr, boost::null_deleter());
       	boost::shared_ptr<std::ofstream> warning_file(boost::make_shared<std::ofstream>(warn_file_name_, std::ios::app));
       	boost::shared_ptr<std::ofstream> debug_file(boost::make_shared<std::ofstream>(debug_file_name_, std::ios::app));

       	/*Each stream become attached to the corresponding backend*/
       	cout_backend -> add_stream(screen_out);
       	cerr_backend -> add_stream(error_out);
       	file_backend -> add_stream(warning_file);
       	debug_backend -> add_stream(debug_file);

       	/*Wrap the backend into the frontend and register it in the core.
	     * The frontend provides synchronization for the backend. We define two frontends in order
	     * to use two different filters. Each one will be connected to each stream to perform
	     * different filter operations.*/
       	boost::shared_ptr<our_frontend> file_sink(boost::make_shared<our_frontend>(file_backend));
       	boost::shared_ptr<our_frontend> cout_sink(boost::make_shared<our_frontend>(cout_backend));
       	boost::shared_ptr<our_frontend> cerr_sink(boost::make_shared<our_frontend>(cerr_backend));
       	boost::shared_ptr<our_frontend> debug_sink(boost::make_shared<our_frontend>(debug_backend));

       	//Defining formatter as a functional in order to use in several sinks
       	boost::log::formatter fmt = boost::log::expressions::stream
	    <<"#"<<_rank
	    <<": ["<< std::setw(6) << std::setiosflags(std::ios::right)
	        << boost::log::expressions::attr< unsigned int >("Counter")
	    <<"]" << std::resetiosflags(std::ios::right)
	    //The next line is a conditional formatter for use only if the log has attributes 'lineID' and 'fileID'
	    //which will happens only for LGS.
            <<boost::log::expressions::if_(boost::log::expressions::has_attr(_lineID) && boost::log::expressions::has_attr(_fileID))
             [
                  boost::log::expressions::stream
                  <<" {"<<_fileID<<":"<<_lineID<<"} "
             ]
            <<" <"<< boost::log::expressions::attr< severity_level >("Severity")
            <<"> "<< boost::log::expressions::message;

	    //Setting the formatter to get formatted output and the filter for each sink.
	    file_sink -> set_formatter(fmt);
       	file_sink -> set_filter(_severity >= SL_WARNING);

       	//The debug messages will go to the same file
       	debug_sink -> set_formatter(fmt);
       	debug_sink -> set_filter(_severity == SL_DEBUG);

       	//Only the log messages from Process 0 will go to the screen.
       	cout_sink -> set_formatter(fmt);
       	cout_sink -> set_filter(_rank==0);

	    //The error messages will output to the cerr console and to the file.
       	cerr_sink -> set_formatter(fmt);
       	cerr_sink -> set_filter(_severity == SL_ERROR);

       	//Registering everything into the logger core
       	core -> add_sink(file_sink);
       	core -> add_sink(cout_sink);
       	core -> add_sink(debug_sink);
       	core -> add_sink(cerr_sink);

       	core -> add_global_attribute("Counter", _counter);
       	core -> add_global_attribute("Rank", rank_order_log);

    }
    /*With this line we return an instance of the severity level logger we will use in kosmos*/
    return boost::log::sources::severity_logger_mt<severity_level>();
}

}

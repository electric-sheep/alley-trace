/*
 * alley-timer.cpp
 *
 *  Created on: Apr 18, 2014
 *      Author: victor, jiri
 */
#include "alley-timer.h"

namespace alley_trace {

GlobalTimer::GlobalTimer(){
    /*REMEMBER: this is the constructor of the GlobalTimer which
     * is a singleton. That means that this is only called at
     * the beginning of the program. It's not called anymore.*/
    tree_ptr=std::make_shared<GlobalTimer::TimerNode>("root");
    actual_timer=tree_ptr;
}

GlobalTimer* GlobalTimer::getInstance(){
    if(!instance_flag){
        global_timer = new GlobalTimer();
        instance_flag = true;
        return global_timer;
    }
    else{
        return global_timer;
    }
}

GlobalTimer::~GlobalTimer(){
    instance_flag = false;
}

void GlobalTimer::open_register(boost::timer::nanosecond_type init_time, std::string received_name){
    std::shared_ptr<GlobalTimer::TimerNode> pTimer_tree=look_for(received_name, actual_timer);
    if(pTimer_tree==NULL){
        //That means that look_for() did not find any node with that name
        //which also means that it does not exists, so, add_node has to be
        //called.
        add_node(init_time, received_name);
    }
    else{
        /*We need to open the already existing node and assign init_time to
         * TimerNode.measure_time. This is the initial time counter that will
         * be used later when closing the node. At that moment, the total time
         * spent by the process associated with the node will be calculated
         * using this value as initial time and the value at closing moment as
         * the final time. That's the reason it is only need to store the initial
         * time.*/
        actual_timer = pTimer_tree;             //pointing to the active node
        actual_timer->init_entry_point = init_time;
    }
}

void GlobalTimer::close_register(boost::timer::nanosecond_type received_time, std::string received_name){
    if(actual_timer->name!="root"){
        if(received_name == actual_timer->name){
            /*The condition means that *actual_timer points to the
             * timer that called this routine. So it means that
             * it is closing now and we only need to register the
             * total time expended.
             * If the previous condition does not hold, some big error
             * has happen.*/
            add_time(received_time);
            //Redirecting actual_timer pointer
            actual_timer=(actual_timer->father_node);
        }

        else{
            /*Throw an error message*/
            LE<<"Error: there is no coincidence between current open timer and closing timer names.";
        }

    }
}

std::shared_ptr<GlobalTimer::TimerNode> GlobalTimer::look_for(std::string node_name, std::shared_ptr<GlobalTimer::TimerNode> tree_pointer){
    /*This function is not recursive because each time a open.node is called
     * that means that this node should be the child of the actual node. So,
     * the program is only interested in the first level of the tree (from the
     * actual open node).*/
    for(unsigned int i=0; i < tree_pointer->children.size(); ++i){
        if(tree_pointer->children[i]->name == node_name){
            /*If true, that means the child node already exists
             * so we will reopen it returning the reference*/
            return (tree_pointer->children[i]);
        }
    }
    //If the node does not exist, it returns null or 0
    return NULL;
}

void GlobalTimer::add_node(boost::timer::nanosecond_type received_init, std::string received_name){
    /*Creating new_child in the tree with initial_time = 0.*/
    /*CRITICAL!!
     * We MUST to implement destroy_tree() function in order to free the heap memory used
     * by the next sentence. Otherwise, we will have a BIG memory leak.*/
    auto new_child = std::make_shared<GlobalTimer::TimerNode>(received_init, received_name);
    actual_timer->children.push_back(new_child);
    /*Now, we need actual_timer points to the still open new node and father points to the previous node
     * for good indirections. After this, father would never be modified again in order to avoid
     * indirection problems.
     * The vector function .back() points to the last element added to the vector. In this case
     * it is the new_child_ just created.*/
    actual_timer->children.back()->father_node=actual_timer;
    /*Now, actual_timer is refreshed in order to point to the active node which is the new one.
     * Due to the fact that children is not a pointer but actual_timer is, we need to pass
     * a reference to it. That way actual_timer will contain a memory address as expected.*/
    actual_timer=(actual_timer->children.back());
 }

void GlobalTimer::print_results(const std::shared_ptr<GlobalTimer::TimerNode> node){
    static int space_num = -1; // We start at the root node which would not be printed, but we want its children to start at space_num=0
    if (node->name != "root"){
        std::string spaces;
        for (int i=0; i<space_num; ++i) spaces.append("  ");
        LI << spaces << "|-- " << node->name << ": " << node->measure_time/(1000*1000) << " ms ";
    }

    ++space_num;

    for(auto it = node->children.begin(); it != node->children.end(); ++it){
        print_results(*it);
    }

    --space_num;//as long as we goes up in the tree we need to decrease the number of spaces before
                 //nodes names
}

void GlobalTimer::add_time(boost::timer::nanosecond_type received_time){
    received_time -= actual_timer->init_entry_point;   //to have the real elapsed time since the open operation
    actual_timer->measure_time+=received_time;
}


bool GlobalTimer::instance_flag = false;
GlobalTimer* GlobalTimer::global_timer = NULL;



PartialTimer::PartialTimer(){//default constructor. Do not create node in GlobalTimer
    gtimer_ = GlobalTimer::getInstance();
    stopped_=0;
}

PartialTimer::PartialTimer(std::string stackname){//constructor that also creates a node
    gtimer_ = GlobalTimer::getInstance();
    /*Starting the timer*/
    cputimer_.stop();
    cputimer_.start();
    init_time_=reading_time();
    value_times_.clear();   //NOT NEEDED I guess
    /*Due to the previous actions, open_register should not need any time value
     * in the call*/
    gtimer_-> open_register(init_time_, stackname);
    stopped_=1;
    opened_nodes_.push_back(stackname);
}

PartialTimer::~PartialTimer(){
    // Close any remaining open nodes
    while (!opened_nodes_.empty()) {
        std::string node_name = opened_nodes_.back();
        gtimer_->close_register(reading_time(), node_name);
        opened_nodes_.pop_back();
        stopped_ -= 1;
    }
    if (stopped_ != 0) {
        LW << "There are " << stopped_ << " nodes without being closed!";
    }
}

void PartialTimer::open(std::string stackname){
    stopped_+=1;
    init_time_ = reading_time();   //taking the initial time value
    value_times_.clear();
    /*Now we call cputimer_.resume() to continue measuring time.
     * EXPLANATION:
     * The cputimer is an object instantiated the first time we create a PartialTimer.
     * So, with this object we pretend to measure the elapsed time of each new node we could create.
     * To do so, it is not needed to use stop() and resume() and the reason is that if we
     * restart the counter each time, we would not have the right result. To see that,
     * think about the first timer we create. If each time we open a child we restart the timer
     * that means that when we close the father, we would not have the total elapsed time. We
     * will only have, when closing the father node, the elapsed time since the last
     * restart. And this is not what is needed.
     *
     * SOLUTIONS:
     * The first one is the approach we are taking here. To take just the time and the beginning
     * of a node opening (could be the creation of the node or a re-openning) and subtract this
     * time to the final time (when closing that node). So we will let the timer runs during all the
     * execution without stops (until we close every nodes when the timer would be likely destroyed).
     *
     * The second approach, taken in a different branch, is to let the timer be an object of the
     * TimerNode structure. So, we will create a new timer object each time we open a node, and this
     * timer will belong only to that node. Doing that way the use of stop(), is_stopped() and resume()
     * are customary.*/
    gtimer_->open_register(init_time_, stackname);
    opened_nodes_.push_back(stackname);
}

void PartialTimer::close(std::string stackname){
    gtimer_->close_register(reading_time(), stackname);
    stopped_-=1;
    if (!opened_nodes_.empty() && opened_nodes_.back() == stackname) {
        opened_nodes_.pop_back(); // Remove the node from the tracking stack
    } else {
        LE << "Error: Mismatch in node names when closing.";
    }
}

boost::timer::nanosecond_type PartialTimer::reading_time(){
    /*Version using boost::timer library.
     * Only called from close() node. That means that, in some point, cputimer.start()
     * has been called and we only need to ask for the elapsed time since then*/
    value_times_=cputimer_.elapsed();
    return value_times_.wall;
}
}


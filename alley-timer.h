/*
 * alley-timer.h
 *
 *  Created on: Nov 26, 2013
 *      Author: victor, jiri
 *
 *  A timer singleton is defined in this header. This singleton will measure the execution time
 *  of different processes and subprocesses. When the execution time of a subprocess is measured,
 *  the timer will add this time to the total time the father process has, in order to know
 *  the total amount of time is consumed.
 *  
 *  The functions and constructors would be defined inside class declarations because
 *  we wan to use a single header utility.
 *
 *  Last Mod.: Nov 8, 2023 by Jiri
 *
 *  Added: boost::timer::cpu_timer object to measure the time expended by
 *          each process.
 */

#pragma once

#include <iostream>
#include <boost/timer/timer.hpp>   //this library is needed to measure time
#include <string>
#include <utility>
#include <vector>

#include "alley-logger.h"


namespace alley_trace {

/** \class GlobalTimer
 * @brief Singleton class to maintain a general tree in which each node will have a name
 * and will have a time value that is the time expended for different processes.
 */
 /** This class will build a multi-node tree in which nodes with names and time values
 * will be created. This class will be a singleton in order to allow multiple processes
 * writing data at the same time.
 * Using the string name we could be able to open previous created nodes and add
 * time to them which means that multi-thread and multi-calling are allowed. The nodes will
 * be used to measure time _span in different processes in the program, contained between the
 * timer.open(std::string name_node) and timer.close(std::string name_node) class functions.
 * These functions belongs to the interface class, @link PartialTimer PartialTimer@endlink,
 * however the control of the nodes is in present GlobalTimer class.
 */
class GlobalTimer{
public:

    /** As a singleton class, it can not contain a constructor but a function
     * that returns a pointer to the singleton object or, if it does not exist,
     * creates the object and return a pointer to it.
     *
     * @return static object of the type *GlobalTimer */
    static GlobalTimer* getInstance();

    /** Change the bool state of instance_flag variable in order to indicate getInstance
     * that the singleton does not exist the next time it would be instantiated.*/
    ~GlobalTimer();

    /** This function will look for some node in the tree under the current
     * node which name coincides with the string passed to the function or it
     * will create the node with that name in there is no coincidence.
     *
     * This function will look for only in the children of the current timer
     * because if open_register() is called at that moment, that means that
     * this new timer corresponds to a process inside the current one.
     *
     *@param init_time boost::timer::nanosecond_type argument that has the initial timer value
     *@param received_name the string that contains the name of the node. It will be used for looking for it when closing*/
    void open_register(boost::timer::nanosecond_type init_time, std::string received_name);

    /** Function that closes the current node and registers the time inside it, moving
     * the pointer to the father's node.
     *
     * @param received_time boost::timer::nanosecond_type value that will be subtracted by init_value to get the expended time
     * @param received_name the name of the node we need to close. This is needed for checking the node before closing.*/
    void close_register(boost::timer::nanosecond_type received_time, std::string received_name);


    ///This function is needed to implement RAII idiom on ~Partial_Timer. @see ~Partial_Timer()
    inline std::string stack_elem_name(){
        //return actual_timer->children.back()->name;
        return actual_timer->name;
    }
    inline void print_tree(){
        print_results(tree_ptr);
    }


private:

    /** \struct TimerNode timer.h "src/timer.h"
     * @brief This is the structure that defines the node of the general tree used in this timer.
     *
     * The only functions it has are the constructors (default, with string name and with both, string name
     * and initial time. There are also two copy-constructors needed because this structure will be the template
     * of a smart pointer that will be part of a STL container (a vector, indeed) and it is compulsory to allows
     * CopyAssignable and CopyConstructible characteristics as is stated in the standard.
     *
     * @param name the name of the node that will be used to find already created nodes and to check if closing the right node.
     * @param init_entry_point the initial time in which it starts measuring
     * @param measure_time time that spans the node and equals final_time-@init_entry_point. final_time will be a value. It's not defined here.
     * @param father_node is a pointer to TimerNode. It points to the father of the present node. Tha is, the node above this one.
     * @param children is a vector of pointers to TimerNode. Has pointers to each subnode created under the present one (that is, its children).
     */
    struct TimerNode
    {
        std::string name;
        boost::timer::nanosecond_type init_entry_point;
        boost::timer::nanosecond_type measure_time; ///< @todo This only stores one type of counter, get rid of it, and replace with cpu_times so we can store all counter types, and only decide what to print at the time of output.
        std::shared_ptr<TimerNode> father_node;
        std::vector<std::shared_ptr<TimerNode>> children;

        TimerNode() = default;
        explicit TimerNode(std::string  name_in)
			: name(std::move(name_in)), init_entry_point(0), measure_time(0) {}
        TimerNode(boost::timer::nanosecond_type time, std::string  name_in)
			: name(std::move(name_in)), init_entry_point(time), measure_time(0) {}
       };

    /** \class GlobalTimer timer.h "src/include/timer.h"
     *
     * @param tree_ is the actual tree of nodes. It is the first node (called root)
     * @param actual_timer is the pointer to TimerNode used to move through all the tree and to operate in it
     * @param instance_flag is a static bool that specifies if the singleton has been instantiated or not
     * @param global is a GlobalTimer* that points to the actual object that is the singleton itself*/
    //TimerNode tree_ptr;
    /*we need this pointer to the real tree. We won't change the place to which the shared_ptr points to.
     * This will be done by actual_timer that will move through the tree nodes, creating and opening them*/
    std::shared_ptr<TimerNode> tree_ptr;
    std::shared_ptr<TimerNode> actual_timer;
    static bool instance_flag;
    static GlobalTimer *global_timer;

    GlobalTimer();

    std::shared_ptr<TimerNode> look_for(std::string node_name, std::shared_ptr<TimerNode> tree_pointer);

    /*Next function add time value to a specific node. It opens the node record and
     * compute the time value in seconds (the clock() function returns clock_t type).*/
    void add_time(boost::timer::nanosecond_type received_time);

    /*This function add a new node to the tree when it happens that there is no string
     * tag that corresponds to any node in our tree. To do so, this function would get
     * the pointer to the actual node (open_timers_.back()) and add a new element to
     * this vector.*/
    void add_node(boost::timer::nanosecond_type received_init, std::string received_name);

    /* We would like the results of each process to be printed. The next function
     * will do that job printing the name of each node and the time expended by it,
     * in a tree structure.*/
    void print_results(std::shared_ptr<TimerNode> node);
};




/***************************************************************************
 * Partial Timer: this class is the one responsible of measuring the real
 * time used in each process. The class will use RAII idiom in order to assure
 * it will finish the time measuring process and pass the values to the global
 * class to add the value to the node tree.
 ***************************************************************************
 * PARTIAL TIMER rebuilt:
 *
 * This new re-design is intended to  allow the class
 * be used just as followed:
 *
 *      //Creates the object
 *      Partial_Timer timer;
 *
 * and calling then
 *
 *      //opens node (new or re-opens)
 *      timer.open(std::string&);
 *
 * and
 *
 *      //closes the node
 *      timer.close(std::string&);
 *
 * Also possible to initialize it with the next syntax
 *
 *      timer(string);
 *
 * MODS: the class will pass the time_read to GlobalTimer functions in
 * order to let them to manage the time values. This class is just
 * an "interface" for the GlobalTimer.
 * *************************************************************************/
class PartialTimer{
public:
    PartialTimer();
    PartialTimer(std::string stackname);
    ~PartialTimer();
    void open(std::string stackname);
    void close(std::string stackname);
    inline void print_times(){
        //call to General_Timer print function
        gtimer_->print_tree();
    }

private:
    /*stopped_ variable is like a reference in the sense that we
     * want to know how much nodes have been created by this object
     * in order to have an account on how much nodes we have to close before leaving.*/
    int stopped_;
    boost::timer::nanosecond_type init_time_; //this value will be the first time point
                                              //and then, it will be subtracted to value_timer_
    boost::timer::cpu_times value_times_;     //struct used for returning the proper values
    GlobalTimer *gtimer_;                    //pointer to the singleton
    boost::timer::cpu_timer cputimer_;        //the boost timer object
    boost::timer::nanosecond_type reading_time();

    std::vector<std::string> opened_nodes_;
};

}
